@extends('master')
@section('judul')
Halaman Tambah Ekstrakurikuler Baru
@endsection

@section('content')

<h1>{{$ekstrakurikuler->jenis}}</h1>
<p>{{$ekstrakurikuler->alasan}}</p>
<a href="/ekstrakurikuler" class="btn btn-secondary btn-sm">kembali</a>
@endsection