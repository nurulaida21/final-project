@extends('master')
@section('judul')
Halaman List Ekstrakurikuler Baru
@endsection

@section('content')

<a href="/ekstrakurikuler/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Jenis</th>
        <th scope="col">Alasan</th>
      </tr>
    </thead>
    <tbody>
    @forelse ($ekstrakurikuler as $key =>$value)
       <tr>
        <td>{{$key+1}}</td>
        <td>{{$value->jenis}}</td>
        <td>{{$value->alasan}}</td>
        <td>
          <form action="/ekstrakurikuler/{{$value->id}}" method="POST">
            @csrf
            @method('DELETE')
            <a href="/ekstrakurikuler/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/ekstrakurikuler/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">

          </form>
        </td>
       </tr>
    @empty
        <tr>
            <td>Tidak Ada Data</td>
        </tr>
    @endforelse

    </tbody>
  </table>


@endsection