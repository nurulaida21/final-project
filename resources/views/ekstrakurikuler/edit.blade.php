@extends('master')
@section('judul')
Halaman Edit Ekstrakurikuler Baru
@endsection

@section('content')
<form action="/ekstrakurikuler/{{$ekstrakurikuler->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Jenis</label>
      <input type="text" name="jenis" value="{{$ekstrakurikuler->jenis}}"class="form-control">
    </div>
    @error('jenis')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    

    <div class="form-group">
      <label>Alasan</label>
      <textarea name="alasan" class="form-control" cols="30" rows="10">{{$ekstrakurikuler->alasan}}</textarea>
    </div>
    @error('alasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection