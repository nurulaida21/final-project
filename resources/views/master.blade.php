<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <title>FINAL PROJECT</title>

  <!-- slider stylesheet -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css" />

  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="{{asset('/template/css/bootstrap.css')}}" />

  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css?family=Baloo+Chettan|Dosis:400,600,700|Poppins:400,600,700&display=swap" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="{{asset('/template/css/style.css')}}" rel="stylesheet" />
  <!-- responsive style -->
  <link href="{{asset('/template/css/responsive.css')}}" rel="stylesheet" />
</head>

<body>
  <div class="hero_area">
    <!-- header section strats -->
    @include('partial.navbar')
    <!-- end header section -->
    <!-- slider section -->
    @yield('judul')
    <!-- end slider section -->
    @yield('content')
  </div>

  <!-- about section -->

  <section class="about_section layout_padding">
    <div class="row">
      <div class="col-md-6 col-lg-4">
        <div class="detail-box">
          <h3>
            @yield('sub-judul')
          </h3>
          <p>
            @yield('sub-content')
          </p>
        </div>
      </div>
    </div>
  </section>

  <!-- end about section -->

  {{-- <!-- business section -->

  <section class="business_section layout_padding">
    <div class="container">
      <h2>
        Gets your business discovered
      </h2>
      <p>
        It is a long established fact that a reader will be distracted by the
        readable content of a page when looking at its layout. The point of
        using Lorem Ipsum is that it has a more-or-less normal distribution of
        letters,
      </p>
    </div>
    <div class="business_container">
      <div class="content-box box-1">
        <div class="detail">
          <h3>
            Not just a website
          </h3>
          <p>
            It is a long established fact that a r <br />
            eader will be distracted by the readable content <br />
            of a page when looking at i <br />
            ts layout. The point of usin <br />
            g Lorem Ipsum is that it has a more-or-less normal <br />
            distribution of letters,
          </p>
        </div>
        <div>
          <a href="">
            read More
          </a>
        </div>
      </div>
      <div class="content-box box-2">
        <div class="detail">
          <h3>
            Hosting & maintenance included
          </h3>
          <p>
            It is a long established fact that a r <br />
            eader will be distracted by the readable content <br />
            of a page when looking at i <br />
            ts layout. The point of usin <br />
            g Lorem Ipsum is that it has a more-or-less normal <br />
            distribution of letters,
          </p>
        </div>
        <div>
          <a href="">
            read More
          </a>
        </div>
      </div>
      <div class="content-box box-3">
        <div class="detail">
          <h3>
            Connect with your customers
          </h3>
          <p>
            It is a long established fact that a r <br />
            eader will be distracted by the readable content <br />
            of a page when looking at i <br />
            ts layout. The point of usin <br />
            g Lorem Ipsum is that it has a more-or-less normal <br />
            distribution of letters,
          </p>
        </div>
        <div>
          <a href="">
            read More
          </a>
        </div>
      </div>
    </div>
  </section>

  <!-- end business section --> --}}


  <!-- footer section -->
  <section class="container-fluid footer_section">
    <p>
      &copy; 2020 All Rights Reserved. Design by
      <a href="https://html.design/">Free Html Templates</a> Distribution <a href="https://themewagon.com">ThemeWagon</a>
    </p>
  </section>
  <!-- footer section -->

  <script type="text/javascript" src="{{asset('/template/js/jquery-3.4.1.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('/template/js/bootstrap.js')}}"></script>

  <script>
    function openNav() {
      document.getElementById("myNav").classList.toggle("menu_width");
      document
        .querySelector(".custom_menu-btn")
        .classList.toggle("menu_btn-style");
    }
  </script>
</body>

</html>