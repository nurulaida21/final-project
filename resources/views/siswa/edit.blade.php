@extends('master')
@section('judul')
Halaman Edit Siswa Baru
@endsection

@section('content')
<form action="/siswa/{{$siswa->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Kelas</label>
      <input type="text" name="kelas" value="{{$siswa->kelas}}"class="form-control">
    </div>
    @error('kelas')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Jurusan</label>
        <input type="text" name="jurusan" value="{{$siswa->jurusan}}"class="form-control">
      </div>
      @error('jurusan')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection