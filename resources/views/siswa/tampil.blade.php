@extends('master')
@section('judul')
Halaman List Siswa Baru
@endsection

@section('content')

<a href="/siswa/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Kelas</th>
        <th scope="col">Jurusan</th>
      </tr>
    </thead>
    <tbody>
    @forelse ($siswa as $key =>$value)
       <tr>
        <td>{{$key+1}}</td>
        <td>{{$value->kelas}}</td>
        <td>{{$value->jurusan}}</td>
        <td>
          <form action="/siswa/{{$value->id}}" method="POST">
            @csrf
            @method('DELETE')
            <a href="/siswa/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/siswa/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">

          </form>
        </td>
       </tr>
    @empty
        <tr>
            <td>Tidak Ada Data</td>
        </tr>
    @endforelse

    </tbody>
  </table>


@endsection