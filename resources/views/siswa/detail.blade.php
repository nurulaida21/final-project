@extends('master')
@section('judul')
Halaman Tambah Siswa Baru
@endsection

@section('content')

<h1>{{$siswa->kelas}}</h1>
<p>{{$siswa->jurusan}}</p>
<a href="/siswa" class="btn btn-secondary btn-sm">kembali</a>
@endsection