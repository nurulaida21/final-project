@extends('master')
@section('judul')
Halaman Tambah Siswa Baru
@endsection

@section('content')
<form action="/siswa" method="POST">
    @csrf
    <div class="form-group">
      <label>Kelas</label>
      <input type="text" name="kelas"class="form-control">
    </div>
    @error('kelas')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Jurusan</label>
        <input type="text" name="jurusan"class="form-control">
      </div>
      @error('jurusan')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection