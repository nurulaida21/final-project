<header class="header_section">
    <div class="container-fluid">
      <nav class="navbar navbar-expand-lg custom_nav-container">
        <a class="navbar-brand" href="index.html">
          <span>
            Sanberschool
          </span>
        </a>

        <div class="navbar-collapse" id="">
          <div class="d-none d-lg-flex ml-auto flex-column flex-lg-row align-items-center mt-3">
            <ul class="navbar-nav ">
              <li class="nav-item">
                <a class="nav-link" href="service.html">
                  <span>Login</span>
                </a>
              </li>
            </ul>
            <form class="form-inline mb-3 mb-lg-0  mr-5">
              <button class="btn  my-sm-0 nav_search-btn" type="submit"></button>
            </form>
          </div>

          <div class="custom_menu-btn">
            <button onclick="openNav()">
              <span class="s-1"> </span>
              <span class="s-2"> </span>
              <span class="s-3"> </span>
              <span class="s-4"> </span>
            </button>
          </div>
          <div id="myNav" class="overlay">
            <div class="overlay-content">
              <a href="/">Home</a>
              <a href="/biodata">Biodata</a>
              <a href="/siswa">Siswa</a>
              <a href="/ekstrakurikuler">Ekstrakurikuler</a>
            </div>
          </div>
        </div>
      </nav>
    </div>
  </header>