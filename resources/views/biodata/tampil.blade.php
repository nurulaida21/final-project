@extends('master')
@section('judul')
Halaman List Biodata Baru
@endsection

@section('content')

<a href="/biodata/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">umur</th>
        <th scope="col">Alamat</th>
      </tr>
    </thead>
    <tbody>
    @forelse ($biodata as $key =>$value)
       <tr>
        <td>{{$key+1}}</td>
        <td>{{$value->nama}}</td>
        <td>{{$value->umur}}</td>
        <td>
          <form action="/biodata/{{$value->id}}" method="POST">
            @csrf
            @method('DELETE')
            <a href="/biodata/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/biodata/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <input type="submit" value="Delete" class="btn btn-danger btn-sm">

          </form>
        </td>
       </tr>
    @empty
        <tr>
            <td>Tidak Ada Data</td>
        </tr>
    @endforelse

    </tbody>
  </table>


@endsection