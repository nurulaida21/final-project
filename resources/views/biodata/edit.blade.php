@extends('master')
@section('judul')
Halaman Edit Biodata Baru
@endsection

@section('content')
<form action="/biodata/{{$biodata->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$biodata->nama}}"class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    
    
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur"value="{{$biodata->umur}}"class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror




    <div class="form-group">
      <label>Alamat</label>
      <textarea name="alamat" class="form-control" cols="30" rows="10">{{$biodata->alamat}}</textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection