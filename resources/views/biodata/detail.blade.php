@extends('master')
@section('judul')
Halaman Tambah Biodata Baru
@endsection

@section('content')

<h1>{{$biodata->nama}}_{{$biodata->umur}}</h1>
<p>{{$biodata->alamat}}</p>
<a href="/biodata" class="btn btn-secondary btn-sm">kembali</a>
@endsection