<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class EkstraController extends Controller
{
    public function create(){
        return view('ekstrakurikuler.tambah');
    }
    public function store(Request $request){
        $validated = $request->validate([
            'jenis' => 'required',
            'alasan' => 'required',
            
        ]);
        DB::table('ekstrakurikuler')->insert([
            'jenis' => $request['jenis'],
            'alasan' => $request['alasan'],
        ]);
        return redirect('/ekstrakurikuler');
    }
    public function index(){
        $ekstrakurikuler = DB::table('ekstrakurikuler')->get();
 
        return view('ekstrakurikuler.tampil', ['ekstrakurikuler' => $ekstrakurikuler]);
      

    }
    public function show($id){
        $ekstrakurikuler  = DB::table('ekstrakurikuler')->where('id',$id)->first();
 
        return view('ekstrakurikuler.detail', ['ekstrakurikuler' => $ekstrakurikuler]);
      
    }
    public function edit($id){
        $ekstrakurikuler = DB::table('ekstrakurikuler')->where('id',$id)->first();
        return view('ekstrakurikuler.edit', ['ekstrakurikuler' => $ekstrakurikuler]);
}
public function update(Request $request, $id){
    
    $request->validate([
        'jenis' => 'required',
        'alasan' => 'required',
    ]);
    $affected= DB::table('ekstrakurikuler')
    ->where('id', $id)
    ->update(
        ['jenis' => $request->jenis,
        'alasan' => $request->alasan,
        ],
    );
    return redirect('/ekstrakurikuler');

}
public function destroy($id){
    DB::table('ekstrakurikuler')->where('id',$id)->delete();
    return redirect('/ekstrakurikuler');

}


}
