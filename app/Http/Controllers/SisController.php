<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SisController extends Controller
{
    public function create(){
        return view('siswa.tambah');
    }
    public function store(Request $request){
        $validated = $request->validate([
            'kelas' => 'required',
            'jurusan' => 'required',
        ]);
        DB::table('siswa')->insert([
            'kelas' => $request['kelas'],
            'jurusan' => $request['jurusan'],
        ]);
        return redirect('/siswa');
    }
    public function index(){
        $siswa = DB::table('siswa')->get();
 
        return view('siswa.tampil', ['siswa' => $siswa]);
      

    }
    public function show($id){
        $siswa  = DB::table('siswa')->where('id',$id)->first();
 
        return view('siswa.detail', ['siswa' => $siswa]);
      
    }
    public function edit($id){
        $siswa = DB::table('siswa')->where('id',$id)->first();
        return view('siswa.edit', ['siswa' => $siswa]);
}
public function update(Request $request, $id){
    
    $request->validate([
        'kelas' => 'required',
        'jurusan' => 'required',
    ]);
    $affected= DB::table('siswa')
    ->where('id', $id)
    ->update(
        ['kelas' => $request->kelas,
        'jurusan' => $request->jurusan,
        ],
    );
    return redirect('/siswa');

}
public function destroy($id){
    DB::table('siswa')->where('id',$id)->delete();
    return redirect('/siswa');

}

}
