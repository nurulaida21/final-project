<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BioController extends Controller
{
    public function create(){
        return view('biodata.tambah');
    }
    public function store(Request $request){
        $validated = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'alamat' => 'required',

        ]);
        DB::table('biodata')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'alamat' => $request['alamat'],
        ]);
        return redirect('/biodata');
    }
    public function index(){
        $biodata = DB::table('biodata')->get();
 
        return view('biodata.tampil', ['biodata' => $biodata]);

    }
    public function show($id){
        $biodata = DB::table('biodata')->where('id',$id)->first();
 
        return view('biodata.detail', ['biodata' => $biodata]);
      
    }
    public function edit($id){
        $biodata = DB::table('biodata')->where('id',$id)->first();
        return view('biodata.edit', ['biodata' => $biodata]);
}
public function update(Request $request, $id){
    
    $request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'alamat' => 'required',
    ]);
    $affected= DB::table('biodata')
    ->where('id', $id)
    ->update(
        ['nama' => $request->nama,
        'umur' => $request->umur,
        'alamat' => $request->alamat],
    );
    return redirect('/biodata');

}
public function destroy($id){
    DB::table('biodata')->where('id',$id)->delete();
    return redirect('/biodata');

}

}
