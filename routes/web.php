<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BioController;
use App\Http\Controllers\SisController;
use App\Http\Controllers\EkstraController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);

// creat data BIODATA
// form tambah 
Route::get('/biodata/create', [BioController::class, 'create']);
// kirim/tambah data ke database
Route::post('/biodata', [BioController::class, 'store']);

// // read data
// // tampil data
Route::get('/biodata', [BioController::class, 'index']);
// detail 
Route::get('/biodata/{biodata_id}', [BioController::class, 'show']);

// update data
// form edit 
Route::get('/biodata/{biodata_id}/edit', [BioController::class, 'edit']);
// form update data
Route::put('/biodata/{biodata_id}', [BioController::class, 'update']);

// delete data
// delete berdasarkan id
Route::delete('/biodata/{biodata_id}', [BioController::class, 'destroy']);


// creat data Siswa
// form tambah 
Route::get('/siswa/create', [SisController::class, 'create']);
// kirim/tambah data ke database
Route::post('/siswa', [SisController::class, 'store']);

// // read data
// // tampil data
Route::get('/siswa', [SisController::class, 'index']);
// detail 
Route::get('/siswa/{siswa_id}', [SisController::class, 'show']);

// update data
// form edit 
Route::get('/siswa/{siswa_id}/edit', [SisController::class, 'edit']);
// form update data
Route::put('/siswa/{siswa_id}', [SisControllerr::class, 'update']);

// delete data
// delete berdasarkan id
Route::delete('/siswa/{siswa_id}', [SisController::class, 'destroy']);


// creat data Ekstrakurikuler
// form tambah 
Route::get('/ekstrakurikuler/create', [EkstraController::class, 'create']);
// kirim/tambah data ke database
Route::post('/ekstrakurikuler', [EkstraController::class, 'store']); 

// // read data
// // tampil data
Route::get('/ekstrakurikuler', [EkstraController::class, 'index']);
// detail 
Route::get('/ekstrakurikuler/{ekstrakurikuler_id}', [EkstraController::class, 'show']);

// update data
// form edit 
Route::get('/ekstrakurikuler/{ekstrakurikuler_id}/edit', [EkstraController::class, 'edit']);
// form update data
Route::put('/ekstrakurikuler/{ekstrakurikuler_id}', [EkstraController::class, 'update']);

// delete data
// delete berdasarkan id
Route::delete('/ekstrakurikuler/{ekstrakurikuler_id}', [EkstraController::class, 'destroy']);
